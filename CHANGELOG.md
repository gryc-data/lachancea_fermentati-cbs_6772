# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-04-28)

### Edited

* Build locus hierarchy.

### Fixed

* Wrong MET in coding gene LAFE_0A01772G.
* Bad gene and mRNA coordinates in locus LAFE_0G10792G.

## v1.0 (2021-04-28)

### Added

* The 7 annotated chromosomes of Lachancea fermentati CBS 6772 (source EBI, [GCA_900074765.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074765.1)).
