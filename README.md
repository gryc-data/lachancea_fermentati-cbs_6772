## *Lachancea fermentati* CBS 6772

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12930](https://www.ebi.ac.uk/ena/browser/view/PRJEB12930)
* **Assembly accession**: [GCA_900074765.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074765.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LAFE0
* **Assembly length**: 10,264,457
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,346,284 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 4753
* **Pseudogene count**: 88
* **tRNA count**: 177
* **rRNA count**: 8
* **Mobile element count**: 17
